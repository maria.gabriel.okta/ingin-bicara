import os
from datetime import timedelta

# APP CONFIG 
APP_NAME = 'inginbicaraEngine'
BASEDIR = os.path.abspath(os.path.dirname(__file__))
DEBUG = True 
SECRET_KEY = os.environ.get('SECRET_KEY')
UPLOAD_FOLDER = '/static/img/profile'

# DB CONFIG
SQLALCHEMY_DATABASE_URI = os.environ.get('MYSQL_URI')
SQLALCHEMY_TRACK_MODIFICATIONS = False

# JWT CONFIG
JWT_SECRET_KEY = os.environ.get('JWT_SECRET_KEY')
JWT_BLACKLIST_ENABLED = True
JWT_BLACKLIST_TOKEN_CHECKS = ['access', 'refresh']
JWT_ACCESS_TOKEN_EXPIRES = timedelta(days=1)
JWT_REFRESH_TOKEN_EXPIRES = timedelta(days=1)
JWT_TOKEN_LOCATION = ['headers', 'cookies']
JWT_ACCESS_COOKIE_PATH = '/'
JWT_REFRESH_COOKIE_PATH = '/token/refresh'
JWT_HEADER_NAME = 'Authorization'
JWT_HEADER_TYPE = 'Bearer'
JWT_ACCESS_COOKIE_NAME = 'auth._token.local'
JWT_REFRESH_COOKIE_NAME = 'auth._refresh_token.local'

# OTHERS
GROUP_CHAT_ID = 4444
