# Ingin Bicara

A web chat application build using flask framework. It's live in DigitalOcean now!
[Check this out~](http://128.199.84.162:9979)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development purpose.

### Prerequisites

What things you need to install the software and how to install them

```
Docker, docker-compose
```

### Installing

A step by step series of examples that tell you how to get a development env running

The step will be

```
git clone https://gitlab.com/maria.gabriel.okta/ingin-bicara.git
```

Build using docker compose

```
docker-compose build
```

Run docker in daemon
```
docker-compose up -d
```

This code doesn't come with db initialization. So you have to create database, intialize table and seed it first before using the app. To intialize table, go to app directory in your container and open python console: 
```
from app import db_mysql
db_mysql.create_all()
```
Seed database with this value
```
Database: inginbicara
User    : admin
Role    :
 - Admin
 - Ordinary
```
