FROM python:3.7-slim-stretch

# RUN echo "nameserver 192.168.1.1" > /etc/resolv.conf

RUN chmod 1777 /tmp

RUN apt update --fix-missing

# RUN apt update 
RUN apt -y upgrade 
RUN apt install -y htop 
RUN apt install -y nano
RUN apt install -y build-essential
RUN apt install -y libssl-dev 
RUN apt install -f libffi-dev
RUN apt-get install -y tzdata
RUN apt install -y default-libmysqlclient-dev

ENV TZ=Asia/Jakarta

# create user 
ARG user=inginbicara
ARG group=inginbicara
ARG uid=1000
ARG gid=1001
RUN adduser ${user}

USER ${user}
RUN mkdir /home/${user}/src
RUN mkdir /home/${user}/log

ADD app/ /home/${user}/src/app/
ADD config.py /home/${user}/src/
ADD requirements.txt /home/${user}/src/

# RUN mkdir /home/${user}/src/app/contentfiles

USER root

# RUN chmod -R a+rw /home/${user}/src/app/contentfiles
RUN pip install -r /home/${user}/src/requirements.txt
CMD ["python", "-u", "/home/inginbicara/src/run.py"]

EXPOSE 9979