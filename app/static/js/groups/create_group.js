$(function(){
    $('button').click(function(){
        var token = localStorage.getItem('token');
        var payload = {
            name: $('#name').val(),
        }
        $.ajax({
            url: '/api/groups/create',
            dataType: 'json',
            data: JSON.stringify(payload),
            contentType: 'application/json',
            type: 'POST',
            headers: { 'Authorization': token},
            success: function(data, status, xhr){
                window.location.href = data.redirect;
            },
            error: function(xhr, status, error){
                var err = JSON.parse(xhr.responseText);
                alert(err.msg)
            }
        });
    });
});