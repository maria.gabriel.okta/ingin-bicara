$(document).ready(function () {
    $('#serverside_table').DataTable({
      bProcessing: true,
      bServerSide: true,
      sPaginationType: "full_numbers",
      lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
      bjQueryUI: true,
      sAjaxSource: '/api/groups/lists',
      columns: [
        {"data": "ID"},
        {
          "data": "Name",
          render: function(data, type, row) {
            data = data + 
            '<div class="links">' + 
            '<a href="/api/groups/join/' + data + '">Join</a> ' + 
            '</div>';                 
            return data;
          }
        }
      ]
    });
  });