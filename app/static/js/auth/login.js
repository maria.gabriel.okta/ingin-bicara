$(function(){
    $('button').click(function(){
        var payload = {
            username: $('#inputUsername').val(),
            password: $('#inputPassword').val()
        }
        $.ajax({
            url: '/api/auth/login',
            dataType: 'json',
            data: JSON.stringify(payload),
            contentType: 'application/json',
            type: 'POST',
            success: function(data, status, xhr){
                localStorage.setItem('token', 'Bearer ' + data.access_token);
                localStorage.setItem('refresh_token', data.refresh_token);
                window.location.href = data.redirect;
            },
            error: function(xhr, status, error){
                var err = JSON.parse(xhr.responseText);
                alert(err.msg)
            }
        });
    });
});

