$(function(){
    $('button').click(function(){
        var payload = {
            email: $('#email').val(),
            username: $('#username').val(),
            name: $('#name').val(),
            password: $('#password').val(),
            re_password: $('#re_password').val(),
        }
        $.ajax({
            url: '/api/cms/signup',
            dataType: 'json',
            data: JSON.stringify(payload),
            contentType: 'application/json',
            type: 'POST',
            success: function(data, status, xhr){
                window.location.href = data.redirect;
            },
            error: function(xhr, status, error){
                var err = JSON.parse(xhr.responseText);
                alert(err.msg)
            }
        });
    });
});