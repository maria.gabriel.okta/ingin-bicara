$(function(){
    $('button').click(function(){
        var username = document.getElementById('username').textContent;
        var payload = {
            name: $('#name').val(),
            password: $('#password').val(),
            re_password: $('#re_password').val(),
        }
        
        $.ajax({
            url: '/api/cms/profile/' + username,
            dataType: 'json',
            data: JSON.stringify(payload),
            contentType: 'application/json',
            type: 'POST',
            success: function(data, status, xhr){
                var err = JSON.parse(xhr.responseText);
                alert(err.msg)
            },
            error: function(xhr, status, error){
                var err = JSON.parse(xhr.responseText);
                alert(err.msg)
            }
        });
    });
});
