$(document).ready(function () {
    $('#serverside_table').DataTable({
      bProcessing: true,
      bServerSide: true,
      sPaginationType: "full_numbers",
      lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
      bjQueryUI: true,
      sAjaxSource: '/api/cms/lists',
      columns: [
        {"data": "ID"},
        {
          "data": "Username",
          render: function(data, type, row) {
            data = data + 
            '<div class="links">' + 
            '<a href="/api/chat/start_chat/' + data + '">Chat</a> ' + 
            '<a href="/api/cms/profile/' + data + '">Edit</a> ' +
            '<a href="/api/cms/delete/' + data + '">Delete</a> ' + 
            '</div>';                 
            return data;
          }
        },
        {"data": "Name"},
        {"data": "Email"},
        {"data": "Active"},
        {"data": "Online"},
        {
            "data": "Photo", 
            render: function(data, type, row) {
                return '<img src="' + data + '" />';
            }
        }
      ]
    });
  });