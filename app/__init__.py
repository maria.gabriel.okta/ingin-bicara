from flask import Flask, jsonify, redirect, url_for, render_template
from flask_bcrypt import Bcrypt
from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager, jwt_required
from flask_socketio import SocketIO
from config import BASEDIR


app = Flask(__name__)
app.config.from_object('config')

db_mysql = SQLAlchemy(app)
jwt = JWTManager(app)
bcrypt = Bcrypt(app)
socketio = SocketIO(app, async_mode='eventlet')

blacklist = set()


@jwt.unauthorized_loader
def my_unauthorized_loader_callback(args):
    return jsonify({
        'msg': 'Auth required.'
    }), 401


@jwt.expired_token_loader
def app_expired_token_callback():
    return jsonify({
        'msg': 'Auth expired.'
    }), 401


@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    return jti in blacklist


@app.route('/', methods=['GET'])
def index():
    return redirect(url_for('api.Authentication_login'))


@app.route('/home', methods=['GET'])
@jwt_required
def home():
    return render_template('index.html')


@app.errorhandler(404)
def page_not_found(e):
    return 'page not found'


from app.api import api_blueprint, api
jwt._set_error_handler_callbacks(api)
app.register_blueprint(api_blueprint)
