from flask import request, session, make_response, render_template, redirect, url_for
from flask_restplus import Namespace, Resource, fields
from flask_socketio import emit, join_room, leave_room
from flask_jwt_extended import get_jwt_identity, jwt_required

from app import socketio
from app.models.mysql import Users, Groups, GroupMembers


api = Namespace('Chats', description='Manage chat')


@socketio.on('joined', namespace='/chat/room')
def joined(message):
    """Sent by clients when they enter a room.
    A status message is broadcast to all people in the room."""
    parsed_members = 'This is private chat.'
    username = session.get('name')
    room_id = session.get('room_id')

    user = Users.query.filter_by(username=username).first()
    if 'group' in room_id:
        group_id = room_id.strip('group-')
        GroupMembers.query.filter_by(group_id=group_id, member_id=user.id).update({'is_active': 1})

        active_members = GroupMembers.query.filter_by(group_id=group_id, is_active=1).all()
        parsed_members = [am.user.username for am in active_members]
        parsed_members = '\n'.join(parsed_members)

    join_room(room_id)
    emit(
        'status', 
        {
            'msg': session.get('name') + ' has entered the room.',
            'members': parsed_members
        }, 
        room=room_id
    )


@socketio.on('text', namespace='/chat/room')
def text(message):
    """Sent by a client when the user entered a new message.
    The message is sent to all people in the room."""
    room_id = session.get('room_id')
    emit('message', {'msg': session.get('name') + ':' + message['msg']}, room=room_id)


@socketio.on('left', namespace='/chat/room')
def left(message):
    """Sent by clients when they leave a room.
    A status message is broadcast to all people in the room."""
    parsed_members = 'This is private chat.'
    username = session.get('name')
    room_id = session.get('room_id')

    user = Users.query.filter_by(username=username).first()
    if 'group' in room_id:
        group_id = room_id.strip('group-')
        GroupMembers.query.filter_by(group_id=group_id, member_id=user.id).update({'is_active': 1})

        active_members = GroupMembers.query.filter_by(group_id=group_id, is_active=1).all()
        parsed_members = [am.user.username for am in active_members]
        parsed_members = '\n'.join(parsed_members)

    leave_room(room_id)
    emit(
        'status', 
        {
            'msg': session.get('name') + ' has left the room.',
            'members': parsed_members
        }, 
        room=room_id
    )


@api.route('/start_chat/<receiver>')
class StartChat(Resource):
    @jwt_required
    def get(self, receiver):
        logged_user = Users.query.get(get_jwt_identity())
        session['name'] = logged_user.username

        if 'group' in receiver:
            group = Groups.query.filter_by(name=receiver.split('-')[1]).first()
            session['room_id'] = '-'.join(['group', str(group.id)])
        else:
            user = Users.query.filter_by(username=receiver).first()
            session['room_id'] = '-'.join(['personal', str(get_jwt_identity() + user.id)])
        
        return redirect(url_for('api.Chats_room_chat'))

@api.route('/room')
class RoomChat(Resource):
    def get(self):
        name = session.get('name', '')
        room_id = session.get('room_id', '')

        if name == '' or room_id == '':
            return redirect(url_for('api.CMS_list_users'))

        return make_response(render_template('chat/chat.html', name=name, room_id=room_id))

