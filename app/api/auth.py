from datetime import datetime

from flask_restplus import Namespace, Resource, fields
from flask import ( 
    request, flash, json, jsonify, render_template, make_response, redirect, 
    url_for
)
from flask_jwt_extended import (
    create_access_token, create_refresh_token, jwt_required, get_raw_jwt,
    get_jwt_identity, jwt_refresh_token_required, set_access_cookies, 
    set_refresh_cookies
)

from sqlalchemy import or_

from app import db_mysql, jwt, blacklist
from app.models.mysql import Users
from app.helpers.hash import HashHelper
from config import JWT_ACCESS_COOKIE_NAME, JWT_REFRESH_COOKIE_NAME

api = Namespace('Authentication', description='You know, for authenticate')

auth_model = api.model('auth_model', {
    'username': fields.String(required=True, description='username/email'),
    'password': fields.String(required=True)
})


@jwt.user_claims_loader
def add_claims_to_access_token(identity):
    # get user and fetch some data to claims 
    logged_user = Users.query.get(identity)
    if logged_user:
        return {
            'id': logged_user.id,
            'username': logged_user.username,
            'role': logged_user.role.name
        }


@api.route('/login')
class Login(Resource):
    def get(self):
        return make_response(render_template('auth/login.html'))

    @api.expect(auth_model, validate=True)
    def post(self):
        payload = request.json

        user = Users.query.filter(or_(Users.username==payload['username'], Users.email==payload['username'])).first()

        if not user:
            return {
                'msg': 'User not found.'
            }, 404

        if user.is_active == 1:
            hash_correct = HashHelper().check_bcrypt_hash(payload['password'], user.password)
            
            if hash_correct:
                access_token = create_access_token(identity=user.id)
                refresh_token = create_refresh_token(identity=user.id)

                user.is_online = 1
                user.last_login_at = datetime.now()
                db_mysql.session.commit()

                resp = make_response(jsonify({
                    'redirect':'/home',
                    'access_token': access_token,
                    'refresh_token': refresh_token
                }))
                set_access_cookies(resp, access_token)
                set_refresh_cookies(resp, refresh_token)
                return resp
            else:
                return {
                    'msg': 'Username and password is not match.'
                }, 401
        else:
            return {
                'msg': 'User inactive.'
            }, 401
        


@api.route('/logout')
class Logout(Resource):
    @jwt_required
    @api.header('Authorization', 'Bearer')
    def get(self):
        logged_user = get_jwt_identity()
        Users.query.filter_by(id=logged_user).update(dict(is_online=0))
        
        jti = get_raw_jwt()['jti']
        blacklist.add(jti)

        return redirect(url_for('api.Authentication_login'))


@api.route('/token/refresh')
class AuthRefreshToken(Resource):
    @jwt_refresh_token_required
    def post(self):
        current_user = get_jwt_identity()
        access_token = create_access_token(identity=current_user)
        
        return {
            'new_access_token': access_token
        }, 200