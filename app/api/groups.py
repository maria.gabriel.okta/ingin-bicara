from flask import request, make_response, render_template, jsonify, url_for, redirect
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restplus import Namespace, Resource, fields, marshal

from app import db_mysql
from app.helpers.slug import GenerateSlug
from app.models.mysql import Groups, GroupMembers


api = Namespace('GroupChat', description='Manage group chat')

group_model = api.model('group_model', {
    'name': fields.String(required=True)
})

list_group_model = api.model('list_group_model', {
    'page': fields.Integer(required=True, default=1),
    'size': fields.Integer(required=True, default=20)
})

group_envelope = {
    'ID': fields.Integer(attribute='id'),
    'Name': fields.String(attribute='name')
}


class DashboardMemberAttr(fields.Raw):
    def format(self, value):
        return {
            'id': value.id,
            'username': value.username,
            'name': value.name,
            'email': value.email,
            'is_online': value.is_online
        }

user_envelope = {
    'is_admin': fields.Boolean(),
    'is_active': fields.Boolean(),
    'users': DashboardMemberAttr(attribute='user')
}



@api.route('/create')
class CreateGroup(Resource):
    @jwt_required
    def get(self):
        return make_response(render_template('groups/create_group.html'))

    @jwt_required
    @api.expect(group_model, validate=True)
    def post(self):
        payload = request.json
        logged_user = get_jwt_identity()

        new_group = Groups.query.filter_by(slug=GenerateSlug().slugify(payload['name'])).first()
        if new_group:
            return {
                'msg': 'Group name already taken.'
            }, 409

        new_group = Groups()
        new_group.name = payload['name']
        new_group.slug = GenerateSlug().slugify(payload['name'])

        try:
            db_mysql.session.add(new_group)
            db_mysql.session.commit()

            group = Groups.query.filter_by(slug=GenerateSlug().slugify(payload['name'])).first()
            gm = GroupMembers()
            gm.member_id = logged_user
            gm.group_id = group.id
            gm.is_admin = 1
            gm.is_active = 1
            
            try:
                db_mysql.session.add(gm)
                db_mysql.session.commit()

                return make_response(jsonify({'redirect':url_for('api.GroupChat_group_index')}))
            except Exception as e:
                # db_mysql.session.delete(group)
                db_mysql.session.commit()

                return {
                    'msg': str(e)
                }, 500
        except Exception as e:
            db_mysql.session.rollback()

            return {
                'msg': str(e)
            }, 500


@api.route('/index')
class GroupIndex(Resource):
    def get(self):
        return make_response(render_template('groups/group_index.html'))


@api.route('/lists')
class ListGroups(Resource):
    @jwt_required
    def get(self):
        request_values = request.values
        start = int(request_values['iDisplayStart'])
        length = int(request_values['iDisplayLength'])

        page = (start / length) + 1
        groups = Groups.query.paginate(page=page, per_page=length, error_out=False)
        data = [marshal(g, group_envelope) for g in groups.items]

        total_data = Groups.query.count()

        output = {
            'sEcho': str(int(request_values['sEcho'])),
            'iTotalRecords': total_data,
            'iTotalDisplayRecords': total_data,
            'data': data
        }

        return jsonify(output)


@api.route('/<name>/members')
class ListGroupMember(Resource):
    @jwt_required
    def get(self):
        group = Groups.query.filter_by(slug=GenerateSlug().slugify(name)).first()
        
        if not group:
            return {
                'msg': 'Group not found.'
            }, 404

        group_members = GroupMembers.query.filter_by(group_id=group.id).all()
        return [marshal(gm, user_envelope) for gm in group_members]


@api.route('/join/<name>')
class JoinGroups(Resource):
    @jwt_required
    def get(self, name):
        logged_user = get_jwt_identity()
        group_room = 'group-{}'

        group = Groups.query.filter_by(slug=GenerateSlug().slugify(name)).first()
        if not group:
            return {
                'msg': 'Group not found.'
            }, 404

        gm = GroupMembers.query.filter_by(group_id=group.id, member_id=logged_user).first()
        if gm:
            if gm.is_active == 1:
                return redirect(url_for('api.Chats_start_chat', receiver=group_room.format(name)))
            else:
                gm.is_active = 1

                try:
                    db_mysql.session.commit()

                    return redirect(url_for('api.Chats_start_chat', receiver=group_room.format(name)))
                except Exception as e:
                    db_mysql.session.rollback()

                    return {
                        'msg': str(e)
                    }, 500
        else:
            new_gm = GroupMembers()
            new_gm.group_id = group.id
            new_gm.member_id = logged_user
            new_gm.is_active = 1

            try:
                db_mysql.session.add(new_gm)
                db_mysql.session.commit()

                return redirect(url_for('api.Chats_start_chat', receiver=group_room.format(name)))
            except Exception as e:
                db_mysql.session.rollback()

                return {
                    'msg': str(e)
                }, 500


@api.route('/leave/<group_id>')
class LeaveGroups(Resource):
    @jwt_required
    def get(self, group_id):
        logged_user = get_jwt_identity()

        if 'group' not in group_id:
            return make_response(render_template('index.html'))
        else:
            group_id = group_id.strip('group-')

        group = Groups.query.filter_by(id=group_id).first()
        if not group:
            return {
                'msg': 'Group not found.'
            }, 404

        gm = GroupMembers.query.filter_by(group_id=group.id, member_id=logged_user).first()
        if gm:
            if gm.is_active == 1:
                gm.is_active = 0

                if gm.is_admin == 1:
                    new_admin = GroupMembers.query.filter_by(group_id=group.id, is_active=1).first()
                    new_admin.is_admin = 1

                try:
                    db_mysql.session.commit()

                    return redirect(url_for('api.GroupChat_group_index'))
                except Exception as e:
                    db_mysql.session.rollback()

                    return {
                        'msg': str(e)
                    }, 500
        
        return {
            'msg': 'You are not a member in this group.'
        }, 400