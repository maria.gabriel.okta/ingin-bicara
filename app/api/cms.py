from flask import request, render_template, make_response, jsonify, url_for
from flask_jwt_extended import jwt_required, get_jwt_identity, get_jwt_claims
from flask_restplus import Namespace, Resource, fields, marshal
from werkzeug.utils import secure_filename
from sqlalchemy.exc import IntegrityError

from app import db_mysql
from app.models.mysql import Users, Roles, GroupMembers
from app.helpers.hash import HashHelper
from config import UPLOAD_FOLDER, GROUP_CHAT_ID


api = Namespace('CMS', description='Manage user')
signup_model = api.model('signup_model', {
    'email': fields.String(required=True),
    'name': fields.String(required=True),
    'username': fields.String(required=True),
    'password': fields.String(required=True),
    're_password': fields.String(required=True)  #TODO: Image
})

list_user_model = api.model('list_user_model', {
    'page': fields.Integer(required=True, default=1),
    'size': fields.Integer(required=True, default=20)
})

update_user_model = api.model('update_user_model', {
    'name': fields.String(),
    'username': fields.String(),
    'password': fields.String(),
    're_password': fields.String()  #TODO: Image
})

user_envelope = {
    'ID': fields.Integer(attribute='id'),
    'Username': fields.String(attribute='username'),
    'Name': fields.String(attribute='name'),
    'Email': fields.String(attribute='email'),
    'Active': fields.String(attribute='is_active'),
    'Online': fields.String(attribute='is_online'),
    'Photo': fields.String(attribute='img_path')
}

ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}


@api.route('/signup')
class SignUp(Resource):
    def get(self):
        return make_response(render_template('auth/signup.html'))

    @api.expect(signup_model, validate=True)
    def post(self):
        payload = request.json
        # pic_file = request.files['profile_picture']

        user = Users.query.filter_by(username=payload['username']).first()
        if user:
            return {
                'msg': 'Username already taken.'
            }, 409

        if payload['password'] != payload['re_password']:
            return {
                'msg': 'Password did not match.'
            }, 401

        # filename = secure_filename(pic_file.filename)
        # pic_path = '/'.join([UPLOAD_FOLDER, filename])
        # pic_file.save(pic_path)

        role = Roles.query.filter_by(slug='ordinary').first()

        user = Users()  #TODO: Img path
        user.email = payload['email']
        user.name = payload['name']
        user.username = payload['username']
        user.password = HashHelper().generate_bcrypt_hash(payload['password'])
        user.role_id = role.id
        user.is_active = 1
        # user.img_path = pic_path

        try:
            db_mysql.session.add(user)
            db_mysql.session.commit()

            return make_response(jsonify({'redirect':url_for('api.Authentication_login')}))
        except IntegrityError:
            db_mysql.session.rollback()
            
            return {
                'msg': 'Email already registered.'
            }, 409
        except Exception as e:
            db_mysql.session.rollback()

            return {
                'msg': str(e)
            }, 500


@api.route('/lists')
class ListUsers(Resource):
    @jwt_required
    def get(self):
        request_values = request.values
        start = int(request_values['iDisplayStart'])
        length = int(request_values['iDisplayLength'])

        page = (start / length) + 1
        users = Users.query.paginate(page=page, per_page=length, error_out=False)
        data = [marshal(u, user_envelope) for u in users.items if u.id != GROUP_CHAT_ID]

        total_data = Users.query.count()

        output = {
            'sEcho': str(int(request_values['sEcho'])),
            'iTotalRecords': total_data,
            'iTotalDisplayRecords': total_data,
            'data': data
        }

        return jsonify(output)


@api.route('/profile/<username>')
class UserProfile(Resource):
    @jwt_required
    def get(self, username):
        logged_user = get_jwt_identity()
        user = Users.query.filter_by(username=username).first()
        role = get_jwt_claims()['role'].lower()
        
        if not user:
            return {
                'msg': 'User not found.'
            }, 404

        if user.id != logged_user:
            if role != 'admin':
                return {
                    'msg': 'You can change your account only.'
                }, 401

        user_data = marshal(user, user_envelope)
        return make_response(render_template('cms/profile.html', data=user_data))

    @api.expect(update_user_model, validate=True)
    def post(self, username):
        payload = request.json

        user = Users.query.filter_by(username=username).first()
        if not user:
            return {
                'msg': 'User not found.'
            }, 404

        if 'username' in payload and payload['username'] != '':
            existing_user = Users.query.filter_by(username=payload['username']).first()
            if existing_user:
                return {
                    'msg': 'Username already taken.'
                }, 409
            else:
                user.username = payload['username']

        if 'password' in payload and payload['password'] != '':
            if 're_password' in payload:
                if payload['password'] != payload['re_password']:
                    return {
                        'msg': 'Password did not match.'
                    }, 401
                else:
                    user.password = HashHelper().generate_bcrypt_hash(payload['password'])
            else:
                return {
                    'msg': 'Input payload validation failed.'
                }, 400

        if 'name' in payload and payload['name'] != '':
            user.name = payload['name']

        try:
            db_mysql.session.commit()

            return {
                'msg': 'Update profile success.'
            }, 200
        except Exception as e:
            db_mysql.session.rollback()

            return{
                'msg': str(e)
            }, 500


@api.route('/delete/<username>')
class DeleteUser(Resource):
    @jwt_required
    def get(self, username):
        logged_user = get_jwt_identity()
        role = get_jwt_claims()['role'].lower()

        if role != 'admin':
            return {
                'msg': 'You are not admin'
            }, 403

        user = Users.query.filter_by(username=username).first()
        if not user:
            return {
                'msg': 'User not found.'
            }, 404

        if user.id == logged_user:
            return {
                'msg': 'You cannot delete yourself'
            }, 401

        user.is_active = 0
        user.is_online = 0
        GroupMembers.query.filter_by(member_id=user.id).update(dict(is_active=0))

        try:
            db_mysql.session.commit()
            return make_response(render_template('index.html'))
        except Exception as e:
            return {
                'msg': str(e)
            }, 500
