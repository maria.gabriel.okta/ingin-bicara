from flask import Blueprint
from flask_restplus import Api

from app.api.auth import api as api_auth
from app.api.cms import api as api_cms
from app.api.groups import api as api_groups
from app.api.chats import api as api_chat

api_blueprint = Blueprint('api', __name__, url_prefix='/api')

api = Api(
    api_blueprint,
    title='inginbicaraAPI',
    version=1,
    description='inginbicara REST API v.1.0',
    doc='/doc/'
)

api.add_namespace(api_auth, path='/auth')
api.add_namespace(api_cms, path='/cms')
api.add_namespace(api_groups, path='/groups')
api.add_namespace(api_chat, path='/chat')
