import re

class GenerateSlug():

	_punct_re = re.compile(r'[\t !"#$%&\'()*\-/<=>?@\[\\\]^_`{|},.]+')

	def slugify(self, text, delim=u'_'):
	    """Generates an ASCII-only slug."""
	    result = []
	    for word in self._punct_re.split(text.lower()):
	        result.append(word)
	    return delim.join(result)