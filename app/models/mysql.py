from datetime import datetime
from app import db_mysql

from sqlalchemy.dialects.mssql import TINYINT


class Users(db_mysql.Model):
    __tablename__ = 'users'

    id = db_mysql.Column(db_mysql.Integer, primary_key=True)
    name = db_mysql.Column(db_mysql.String(128))
    username = db_mysql.Column(db_mysql.String(128), unique=True)
    email = db_mysql.Column(db_mysql.String(255), unique=True)
    password = db_mysql.Column(db_mysql.String(255))
    last_login_at = db_mysql.Column(db_mysql.DateTime)
    is_active = db_mysql.Column(TINYINT(), default=0)
    is_online = db_mysql.Column(TINYINT(), default=0)
    img_path = db_mysql.Column(db_mysql.String(255))
    created_at = db_mysql.Column(db_mysql.DateTime, default=datetime.now())

    role_id = db_mysql.Column(db_mysql.Integer, db_mysql.ForeignKey('roles.id'))
    role = db_mysql.relationship('Roles', backref='user', lazy=True, foreign_keys=role_id)


class Roles(db_mysql.Model):
    __tablename__ = 'roles'

    id = db_mysql.Column(db_mysql.Integer, primary_key=True)
    name = db_mysql.Column(db_mysql.String(50))
    slug = db_mysql.Column(db_mysql.String(75), unique=True)
    created_at = db_mysql.Column(db_mysql.DateTime, default=datetime.now())


members_assoc_table = db_mysql.Table('group_members',
    db_mysql.Column('group_id', db_mysql.Integer, db_mysql.ForeignKey('groups.id'), primary_key=False),
    db_mysql.Column('member_id', db_mysql.Integer, db_mysql.ForeignKey('users.id'), primary_key=False),
)


class Groups(db_mysql.Model):
    __tablename__ = 'groups'

    id = db_mysql.Column(db_mysql.Integer, primary_key=True)
    name = db_mysql.Column(db_mysql.String(50))
    slug = db_mysql.Column(db_mysql.String(75), unique=True)
    created_at = db_mysql.Column(db_mysql.DateTime, default=datetime.now())

    members = db_mysql.relationship('Users', secondary=members_assoc_table, lazy='subquery', backref=db_mysql.backref('GroupMembers', lazy=True))


class GroupMembers(db_mysql.Model):
    __tablename__ = 'group_members'
    __table_args__ = {'extend_existing': True}

    id = db_mysql.Column(db_mysql.Integer, primary_key=True)
    group_id = db_mysql.Column(db_mysql.Integer, db_mysql.ForeignKey('groups.id'))
    member_id = db_mysql.Column(db_mysql.Integer, db_mysql.ForeignKey('users.id'))
    is_active = db_mysql.Column(TINYINT(), default=0)
    is_admin = db_mysql.Column(TINYINT(), default=0)
    created_at = db_mysql.Column(db_mysql.DateTime, default=datetime.now())

    user = db_mysql.relationship('Users', backref='group_member', lazy=True, foreign_keys=member_id)


class Chats(db_mysql.Model):
    __tablename__ = 'chats'

    id = db_mysql.Column(db_mysql.Integer, primary_key=True)
    sender_id = db_mysql.Column(db_mysql.Integer, db_mysql.ForeignKey('users.id'))
    receiver_id = db_mysql.Column(db_mysql.Integer, db_mysql.ForeignKey('users.id'))
    message = db_mysql.Column(db_mysql.Text)
    created_at = db_mysql.Column(db_mysql.DateTime, default=datetime.now())

    sender = db_mysql.relationship('Users', backref='chats_sender', lazy=True, foreign_keys=sender_id)
    receiver = db_mysql.relationship('Users', backref='chats_receiver', lazy=True, foreign_keys=receiver_id)