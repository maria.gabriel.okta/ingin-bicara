from app import app
from app import socketio

if __name__ == "__main__":
    # app.run(host='127.0.0.1', port=9979, debug=True)
    socketio.run(app, host='0.0.0.0', port=9979, debug=True)